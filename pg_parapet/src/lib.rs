use pgrx::guc::*;
use pgrx::prelude::*;

pgrx::pg_module_magic!();

static mut HOOKS: ParapetHooks = ParapetHooks {};

static GUC_FORCE_WHERE_ON_DELETE: GucSetting<bool> = GucSetting::<bool>::new(false);
static GUC_FORCE_WHERE_ON_UPDATE: GucSetting<bool> = GucSetting::<bool>::new(false);
static GUC_ONLY_SUPERUSERS_CAN_TRUNCATE: GucSetting<bool> = GucSetting::<bool>::new(false);

struct ParapetHooks {
}

impl pgrx::hooks::PgHooks for ParapetHooks {
    fn post_parse_analyze(
        &mut self,
        parse_state: PgBox<pg_sys::ParseState>,
        query: PgBox<pg_sys::Query>,
        jumble_state: Option<PgBox<pgrx::JumbleState>>,
        prev_hook: fn(
            parse_state: PgBox<pg_sys::ParseState>,
            query: PgBox<pg_sys::Query>,
            jumble_state: Option<PgBox<pgrx::JumbleState>>,
        ) -> pgrx::hooks::HookResult<()>,
    ) -> pgrx::hooks::HookResult<()> {
            debug1!("Parapet: Query hooked");
            match query.commandType {
                pg_sys::CmdType_CMD_DELETE => check_delete(&query),
                pg_sys::CmdType_CMD_UPDATE => check_update(&query),
                _ => ()
            }

            prev_hook(parse_state, query, jumble_state)
    }

    // Every time the commande is not SELECT,INSERT,UPDATE,DELETE
    // it is handled by the process_utility() function
    fn process_utility_hook(
        &mut self,
        pstmt: PgBox<pg_sys::PlannedStmt>,
        query_string: &core::ffi::CStr,
        read_only_tree: Option<bool>,
        context: pg_sys::ProcessUtilityContext,
        params: PgBox<pg_sys::ParamListInfoData>,
        query_env: PgBox<pg_sys::QueryEnvironment>,
        dest: PgBox<pg_sys::DestReceiver>,
        completion_tag: *mut pg_sys::QueryCompletion,
        prev_hook: fn(
            pstmt: PgBox<pg_sys::PlannedStmt>,
            query_string: &core::ffi::CStr,
            read_only_tree: Option<bool>,
            context: pg_sys::ProcessUtilityContext,
            params: PgBox<pg_sys::ParamListInfoData>,
            query_env: PgBox<pg_sys::QueryEnvironment>,
            dest: PgBox<pg_sys::DestReceiver>,
            completion_tag: *mut pg_sys::QueryCompletion,
        ) -> pgrx::hooks::HookResult<()>,
    ) -> pgrx::hooks::HookResult<()> {
        check_truncate(&pstmt);
        prev_hook(
            pstmt,
            query_string,
            read_only_tree,
            context,
            params,
            query_env,
            dest,
            completion_tag,
        )
    }
}



fn _check_delete_1(query: &PgBox<pg_sys::Query>) {
    debug1!("Parapet: checking the DELETE query ! (not really)");
    debug2!("Parapet: query = {:#?}",query);
}

fn _check_delete_2(query: &PgBox<pg_sys::Query>) {
    if ! has_where(query) {
        panic!("Parapet: DELETE queries must have a WHERE clause");
    }
}

fn check_delete(query: &PgBox<pg_sys::Query>) {
    if GUC_FORCE_WHERE_ON_DELETE.get() && ! has_where(query) {
        panic!("Parapet: DELETE queries must have a WHERE clause");
    }
}

fn check_update(query: &PgBox<pg_sys::Query>) {
    if GUC_FORCE_WHERE_ON_UPDATE.get() && ! has_where(query) {
        panic!("Parapet: UPDATE queries must have a WHERE clause");
    }
}

fn check_truncate(pstmt: &PgBox<pg_sys::PlannedStmt>) {
    unsafe {
        if  GUC_ONLY_SUPERUSERS_CAN_TRUNCATE.get() &&
            pgrx::is_a(pstmt.utilityStmt, pg_sys::NodeTag::T_TruncateStmt) &&
            ! pg_sys::superuser() {
                panic!("Parapet: Only Superusers can truncate")
        }
    }
}

fn has_where(query: &PgBox<pg_sys::Query>) -> bool {
    // The "jointree" is a raw pointer
    // as we dereference that raw pointer, we take take the responsibility that
    // it may point to an incorrect location.
    // This is why we need to make this operation as `unsafe`
    let jointree = unsafe { *query.jointree };
    debug2!("Parapet: jointree = {:#?}", jointree);
    if jointree.quals.is_null() {
        return false;
    }
    true
}


#[pg_guard]
pub unsafe extern "C" fn _PG_init() {
    pgrx::hooks::register_hook(&mut HOOKS);

    GucRegistry::define_bool_guc(
            "pg_parapet.force_where_on_delete",
            "Activate the Parapet protection on DELETE commands",
            "DELETE without a WHERE clause will be forbidden",
            &GUC_FORCE_WHERE_ON_DELETE,
            GucContext::Suset,
            GucFlags::default(),
    );

    GucRegistry::define_bool_guc(
            "pg_parapet.force_where_on_update",
            "Activate the Parapet protection on UPDATE commands",
            "UPDATE without a WHERE clause will be forbidden",
            &GUC_FORCE_WHERE_ON_UPDATE,
            GucContext::Suset,
            GucFlags::default(),
    );

    GucRegistry::define_bool_guc(
            "pg_parapet.only_superusers_can_truncate",
            "Activate the Parapet protection on TRUNCATE commands",
            "TRUNCATE will be restricted to superusers",
            &GUC_ONLY_SUPERUSERS_CAN_TRUNCATE,
            GucContext::Suset,
            GucFlags::default(),
    );

    debug1!("Parapet: extension initialized");
}

#[cfg(any(test, feature = "pg_test"))]
#[pg_schema]
mod tests {
    use pgrx::prelude::*;


    #[pg_test]
    unsafe fn test_delete() {
        Spi::run("CREATE TABLE t AS SELECT 1;").expect("SPI Failed");
        Spi::run("DELETE FROM t").unwrap();
    }

    #[pg_test]
    unsafe fn test_delete_with_where_is_accepted() {
        Spi::run("SET pg_parapet.force_where_on_delete to true;").expect("SPI Failed");
        Spi::run("CREATE TABLE t AS SELECT 1;").expect("SPI Failed");
        Spi::run("DELETE FROM t WHERE 1=1;").unwrap();
    }

    #[pg_test]
    #[should_panic]
    unsafe fn test_delete_without_where_is_rejected() {
        Spi::run("SET pg_parapet.force_where_on_delete to true;").expect("SPI Failed");
        Spi::run("CREATE TABLE t AS SELECT 1;").expect("SPI Failed");
        Spi::run("DELETE FROM t").unwrap();
    }

}

/// This module is required by `cargo pgrx test` invocations.
/// It must be visible at the root of your extension crate.
#[cfg(test)]
pub mod pg_test {
    pub fn setup(_options: Vec<&str>) {
        // perform one-off initialization when the pg_test framework starts
    }

    pub fn postgresql_conf_options() -> Vec<&'static str> {
        // return any postgresql.conf settings that are required for your tests
        vec![]
    }
}
