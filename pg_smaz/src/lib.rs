extern crate smaz;

use pgrx::prelude::*;
use pgrx::{InOutFuncs, StringInfo};
use serde::{Deserialize, Serialize};

pgrx::pg_module_magic!();

#[derive(PostgresType, Serialize, Deserialize, Debug, PartialEq)]
#[inoutfuncs]
// Vec<u8> is the rust equivalent of a BYTEA
pub struct Smaz (Vec<u8>);

/// Implement the InOutFuncs trait to provide 
/// our own text input and output functions
impl InOutFuncs for Smaz {

    // parse the provided CStr into a Smaz
    fn input(input: &core::ffi::CStr) -> Self {
        let val = smaz::compress(input.to_str().expect("Wrong Input").as_bytes());
        Smaz(val)
    }

    // Output ourselves as text into the provided `StringInfo` buffer
    fn output(&self, buffer: &mut StringInfo) {
        // we compressed the value before, so `decrompress` should not fail :)
        // and we can use `unwrap` here
        let val = smaz::decompress(&self.0).unwrap();
        buffer.push_str(&String::from_utf8(val).unwrap());
    }
}

#[pg_extern]
fn length(val: Smaz) -> i32 {
    return val.0.len().try_into().unwrap();
}

#[pg_extern]
fn raw(val: Smaz) -> Vec<u8> {
    return val.0;
}


