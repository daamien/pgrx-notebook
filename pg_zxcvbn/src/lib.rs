extern crate zxcvbn;

use pgrx::prelude::*;

pgrx::pg_module_magic!();

#[pg_extern]
fn zxcvbn(password: String) -> String {
    let estimate = zxcvbn::zxcvbn(&password, &[]).unwrap();
    let score = estimate.score() as i8;
    if score < 3 {
        let feedback = estimate.feedback().clone().unwrap();
        info!("{}", feedback.warning().unwrap());
        for s in feedback.suggestions().iter() {
            info!("{}", s);
        }
    }
    return score.to_string();
}

#[cfg(any(test, feature = "pg_test"))]
#[pg_schema]
mod tests {
    use pgrx::prelude::*;

    #[pg_test]
    fn test_pg_zxcvbn() {
        assert_eq!("4", crate::zxcvbn("correcthorsebatterystaple".to_string()));
    }

}

/// This module is required by `cargo pgrx test` invocations.
/// It must be visible at the root of your extension crate.
#[cfg(test)]
pub mod pg_test {
    pub fn setup(_options: Vec<&str>) {
        // perform one-off initialization when the pg_test framework starts
    }

    pub fn postgresql_conf_options() -> Vec<&'static str> {
        // return any postgresql.conf settings that are required for your tests
        vec![]
    }
}
