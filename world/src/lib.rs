use pgrx::prelude::*;

pgrx::pg_module_magic!();

#[pg_extern]
fn hello_world() -> &'static str {
    "Hello, world"
}

#[pg_extern]
fn hello(name: &str) -> String {
    format!("Hello, {name}")
}

#[pg_extern]
fn hello2(name: &str) -> String {
    if name.is_empty(){
        panic!("name is empty");
    }
    format!("Hello, {name}")
}

#[pg_extern]
fn hello3(name: &str) -> String {
    let hello : &str = match name {
        "Pierre"            => "Bonjour",
        "Diego" | "Maya"    => "¡Hola!",
        _                   => "Hello",
    };
    format!("{hello}, {name}")
}

#[cfg(any(test, feature = "pg_test"))]
#[pg_schema]
mod tests {
    use pgrx::prelude::*;

    #[pg_test]
    fn test_hello_world() {
        assert_eq!("Hello, world", crate::hello_world());
    }

}

/// This module is required by `cargo pgrx test` invocations.
/// It must be visible at the root of your extension crate.
#[cfg(test)]
pub mod pg_test {
    pub fn setup(_options: Vec<&str>) {
        // perform one-off initialization when the pg_test framework starts
    }

    pub fn postgresql_conf_options() -> Vec<&'static str> {
        // return any postgresql.conf settings that are required for your tests
        vec![]
    }
}
