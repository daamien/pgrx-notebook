# Créer une extension Postgres en Rust

![Photo Credit Miguel A Padrinan](./images/pexels-miguel-a-padrinan-114108.jpg)

<!-- BEGIN preambule -->


## Préambule

Cet article fait partie d'une série consacrée au framework [PGRX],
un environnement de développement qui facilite la conception
d'extensions PostgreSQL avec le langage [Rust].

[Rust]: https://stackoverflow.blog/2020/01/20/what-is-rust-and-why-is-it-so-popular/
[PGRX]: https://github.com/pgcentralfoundation/pgrx

Retrouvez les autres articles de la série ci-dessous:

* [Episode 1 : Un nouveau monde](/01-hello/)
* [Episode 2 : Demander le luhn](/02-luhn/)
* [Episode 3 : Le chant du SIREN](/03-siren/)
* [Episode 4 : Accrocher le parapet](/04-hooks/)


[Episode 1 : Un nouveau monde]: /01-hello/
[Episode 2 : Demander le luhn]: /02-luhn/
[Episode 3 : Le chant du SIREN]: /03-siren/

> Important: Cette série d'articles n'est pas une introduction au langage Rust !
> Il existe d'autres ressources pour ça (notamment [ici] et [là]). On se
> focalisera ici sur la mécanique interne d'une extension Postgres.

[ici]: https://stevedonovan.github.io/rust-gentle-intro/
[là]: https://doc.rust-lang.org/rust-by-example/

Chaque épisode est conçu en 2 parties : un tutoriel de 30 minutes environ, suivi
d'exercices pour aller plus loin. Les exercices sont optionnels.

[PGXS]: https://www.postgresql.org/docs/current/extend-pgxs.html

<!-- END preambule -->

Pour chaque épisode, un exemple d'implémentation est fourni dans le dépot gitlab:

<https://gitlab.com/daamien/pgrx-tuto>


## Ressources complémentaires

* [Le "Rust Book" en français](https://jimskapt.github.io/rust-book-fr/)
* [Rust Playground](https://play.rust-lang.org/)
* [PostgresML is Moving to Rust for our 2.0 Release](https://postgresml.org/blog/postgresml-is-moving-to-rust-for-our-2.0-release)
* [Was Rust Worth it ?](https://jsoverson.medium.com/was-rust-worth-it-f43d171fb1b3)
* [Traduction des termes](https://jimskapt.github.io/rust-book-fr/translation-terms.html)
