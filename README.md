
Ce dépot contient une série d'extensions PostgreSQL developpées avec PGRX.

## Avertissement 

Ces extensions ont été conçues dans un cadre pédagogique pour étudier comment
créer des extensions avec Rust. Elles ne sont pas supportées et elles ne sont pas maintenues.

## Articles

* Ecrire une extension Postgres en Rust - Episode 1 : Bonjour le monde
* Ecrire une extension Postgres en Rust - Episode 2 : Demander le luhn
* Ecrire une extension Postgres en Rust - Episode 3 : Le chant du SIREN
