use core::ffi::CStr;
use pgrx::prelude::*;
use pgrx::{pgrx, InOutFuncs, StringInfo};
use rand::Rng;
use serde::{Deserialize, Serialize};

pgrx::pg_module_magic!();

/// standard Rust equality/comparison derives
#[derive(Eq, PartialEq, Ord, Hash, PartialOrd)]

#[derive(PostgresType, Serialize, Deserialize, Debug)]
/// automatically generate =, <> SQL operator functions
#[derive(PostgresEq)]

/// automatically generate <, >, <=, >=, and a "_cmp" SQL functions
/// When "PostgresEq" is also derived, pgrx also creates an "opclass" (and family)
/// so that the type can be used in indexes `USING btree`
#[derive(PostgresOrd)]

/// automatically generate a "_hash" function, and the necessary "opclass" (and family)
/// so the type can also be used in indexes `USING hash`
#[derive(PostgresHash)]

#[inoutfuncs]
pub struct Siren(i32);

impl InOutFuncs for Siren {
    // Parse the provided CStr into a Siren
    //   - Remove all spaces
    //   - Check the luhn checksum
    //   - Remove the luhn digit (division by 10)
    fn input(input: &CStr) -> Self {
        let val = input.to_str().expect("Invalid Input").replace(' ', "");
        if !luhn3::decimal::valid(&val.clone().into_bytes()) {
            error!("{}", "Not a valid SIREN number");
        }
        Siren(val.parse::<i32>().expect("value should be an number") / 10)
    }

    // Output ourselves as text into the provided `StringInfo` buffer
    //   - Split the Integer value (self.0) in 3 parts
    //   - Add the luhn cheksum at the end
    fn output(&self, buffer: &mut StringInfo) {
        let part1 = self.0 / 100000 % 1000;
        let part2 = self.0 / 100 % 1000;
        let part3 = self.0 % 100;
        let part4 = luhn3::decimal::checksum(&self.0.to_string().into_bytes())
            .expect("Failed to compute the checksum") as char;
        let val = format!("{part1:03} {part2:03} {part3:02}{part4}");
        buffer.push_str(val.as_str());
    }
}

#[pg_extern]
fn random_siren() -> Siren {
    let mut rng = rand::thread_rng();
    Siren(rng.gen_range(0..100000000))
}

#[derive(Serialize, Deserialize, PostgresType)]
#[derive(Eq, PartialEq, Ord, Hash, PartialOrd)]
#[derive(PostgresEq)]
#[derive(PostgresOrd)]
#[derive(PostgresHash)]
#[inoutfuncs]
pub struct Siret {
    siren: Siren,
    etab: i32
}

impl InOutFuncs for Siret {

    // Parse the provided CStr into a Siret
    fn input(input: &CStr) -> Self {
        let val = input.to_str().expect("Invalid Input").replace(' ', "");
        let first_nine = val[..8].parse::<i32>().expect("value should be an number");
        let last_five  = val[9..].parse::<i32>().expect("value should be an number");
        Siret{
            siren: Siren(first_nine),
            etab:  last_five
        }
    }

    // Output ourselves as text into the provided `StringInfo` buffer
    fn output(&self, buffer: &mut StringInfo) {
        let part1 = self.siren.0 / 100000 % 1000;
        let part2 = self.siren.0 / 100 % 1000;
        let part3 = self.siren.0 % 100;
        let part4 = luhn3::decimal::checksum(&self.siren.0.to_string().into_bytes())
            .expect("Failed to compute the checksum") as char;
        let part5 = self.etab;
        let val = format!("{part1:03} {part2:03} {part3:02}{part4} {part5:05}");
        buffer.push_str(val.as_str());
    }
}

#[pg_extern]
fn hello_pg_siren() -> &'static str {
    "Hello, pg_siren"
}

#[cfg(any(test, feature = "pg_test"))]
#[pg_schema]
mod tests {
    use pgrx::prelude::*;

    #[pg_test]
    unsafe fn test_input() {
        Spi::run("SELECT '483247862'::SIREN").unwrap();
    }

    #[pg_test]
    unsafe fn test_create_table() {
        Spi::run("CREATE TABLE company (id SIREN PRIMARY KEY)").unwrap();
    }

    #[pg_test]
    #[should_panic]
    unsafe fn test_wrong_input() {
        Spi::run("SELECT '999 999 999'::SIREN").unwrap();
    }

    #[pg_test]
    unsafe fn test_random_siren() {
        Spi::run("SELECT random_siren()").unwrap();
    }

    #[pg_test]
    unsafe fn test_input_siret() {
        Spi::run("SELECT '48324786200053'::SIRET").unwrap();
    }

    #[pg_test]
    #[should_panic]
    unsafe fn test_wrong_input_siret() {
        Spi::run("SELECT 'should fail'::SIRET").unwrap();
    }

}

/// This module is required by `cargo pgrx test` invocations.
/// It must be visible at the root of your extension crate.
#[cfg(test)]
pub mod pg_test {
    pub fn setup(_options: Vec<&str>) {
        // perform one-off initialization when the pg_test framework starts
    }

    pub fn postgresql_conf_options() -> Vec<&'static str> {
        // return any postgresql.conf settings that are required for your tests
        vec![]
    }
}
