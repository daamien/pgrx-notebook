use pgrx::prelude::*;

pgrx::pg_module_magic!();

#[pg_extern]
fn hello_pg_luhn() -> &'static str {
    "Hello, pg_luhn"
}

#[pg_extern]
fn luhn_checksum(input: &str) -> char {
    luhn3::decimal::checksum(input.as_bytes()).expect("Input should be decimal") as char
}

#[pg_extern]
fn luhn_valid(input: &str) -> bool {
    luhn3::decimal::valid(input.as_bytes())
}

#[pg_extern]
fn luhn_append(input: &str) -> String {
    let checksum = luhn_checksum(input);
    format!("{input}{checksum}")
}

#[cfg(any(test, feature = "pg_test"))]
#[pg_schema]
mod tests {
    use pgrx::prelude::*;

    #[pg_test]
    fn test_luhn_checksum() {
        assert_eq!("8", crate::luhn_checksum("1"));
    }

    #[pg_test]
    fn test_luhn_append() {
        assert_eq!("18", crate::luhn_append("1"));
    }

    #[pg_test]
    fn test_luhn_valid() {
        assert!(crate::luhn_valid("0"));
        assert!(crate::luhn_valid("18"));
        assert!(crate::luhn_valid("109"));
        assert!(!crate::luhn_valid("1234"));
    }

    #[pg_test]
    fn test_hello_pg_luhn() {
        assert_eq!("Hello, pg_luhn", crate::hello_pg_luhn());
    }
}

/// This module is required by `cargo pgrx test` invocations.
/// It must be visible at the root of your extension crate.
#[cfg(test)]
pub mod pg_test {
    pub fn setup(_options: Vec<&str>) {
        // perform one-off initialization when the pg_test framework starts
    }

    pub fn postgresql_conf_options() -> Vec<&'static str> {
        // return any postgresql.conf settings that are required for your tests
        vec![]
    }
}
